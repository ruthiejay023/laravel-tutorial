<x-dropdown>
    <x-slot name="trigger">
        <button 
            class="flex lg:inline-flex py-2 pl-3 pr-6 text-sm font-semibold w-full lg:w-32 text-left"
        >
        {{ isset($currentCategory) ? ucwords($currentCategory->name) : 'Categories'}}

          <x-down-arrow class="absolute pointer-events-none" style="right: 12px;" width="22" height="22" />
        </button>
    </x-slot>
    <x-dropdown-item href="/?{{ http_build_query(request()->except('category', 'page')) }}" :active="request()->routeIs('home')">All</x-dropdown-item>
    @foreach ($categories as $category)
    <x-dropdown-item 
        href="/?category={{ $category->slug }}&{{ http_build_query(request()->except('category', 'page')) }}"
        :active='request()->is("/?category={$category->slug}")'
    >{{ ucwords($category->name) }}</x-dropdown-item>
    @endforeach
</x-dropdown>